import Vue from 'vue';
const request = {
    async implicit ( url: string, body: any, options: { headers?: any, method?: string } = {} ) {
        return await fetch( url, {
            method: options.method || 'post',
            headers: {
                Authorization: `client_id ${process.env.CLIENT_ID}`,
                ...options.headers
            },
            body
        } ).then( async ( resp: any ) => {
            if ( resp.status === 200 ) {
                const { data } = await resp.json();
                return data;
            }
            throw await resp.json();
        } );
    }
};
const RequestPlugin = {
    install ( Vue: any ) {
        Vue.prototype.$request = request;
    }
};
Vue.use( RequestPlugin );