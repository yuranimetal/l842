import Vuex from 'vuex';

export const state = () => ( {
    optionsMenu: {
        login: true,
        register: true,
        search: true
    },
    serviceDetail: null,
    cartServices: [],
    benefits: [],
    benefit: null,
    titulosDetail: []
} );



export const mutations = {
    setOptionsMenu ( state: any, optionsMenu: any ) {
        state.optionsMenu = optionsMenu;
    },
    serviceDetail (state:any, service:any) {
        state.serviceDetail = service;
    },
    setCartServices (state:any, services:any) {
        state.cartServices = services;
    },
    removeCartService (state:any, index:number) {
        state.cartServices.splice(index, 1);
    },
    removeCartServiceOfProvider (state:any, service: any) {
        state.cartServices.pop();
    },
    addCartService (state:any, service:any) {
        state.cartServices.push(service);
    },
    benefitDetail (state:any, benefit:any) {
        state.benefit = benefit;
    },
    setCartBenefits (state:any, benefits:any) {
        state.benefits = benefits;
    },
    setTitulosDetail (state:any, titulos:any) {
      state.titulosDetail = titulos;
  },
};
